import math

fuel = list()
input_file = open("input.txt", "r")

# Loop through the file, one line at a time
for line in input_file:

    # Eliminate line endings
    mass = line.rstrip()

    module_fuel = math.floor(int(mass) / 3) - 2

    while module_fuel > 0:
        fuel.append(module_fuel)

        module_fuel = math.floor(module_fuel / 3) - 2

print(sum(fuel))

input_file.close()
