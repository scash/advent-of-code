import math

fuel = 0
input_file = open("input.txt", "r")

# Loop through the file, one line at a time
for line in input_file:

    # Eliminate line endings
    mass = line.rstrip()

    fuel += math.floor(int(mass) / 3) - 2

print(fuel)

input_file.close()
