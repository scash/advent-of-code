input_file = open("input.txt", "r")

# Read input and eliminate line ending
instructions = input_file.read().rstrip()

# Obtain list of values
instructions = instructions.split(",")

# Convert all values to integers
instructions = list(map(int, instructions))

output = list()
error_encountered = False
opcode_position, input_val = 0, 1

while instructions[opcode_position] != 99:

    # Default mode of "position" for all parameters
    modes = (0, 0, 0)

    # Retrieve opcode
    opcode = instructions[opcode_position]
    str_opcode = str(opcode)

    # Encountered opcode that contains parameter modes
    if len(str_opcode) > 1:

        # If applicable, fill missing position modes
        modes_opcode = str_opcode.zfill(5)

        opcode = int(modes_opcode[4])

        # Obtain parameter modes, however, transform to
        #   be left to right instead of right to left
        modes = tuple(map(int, reversed(modes_opcode[:3])))

    if opcode in [1, 2]:

        # Retrieve left operand value based on mode type
        if modes[0]:
            left_operand = instructions[opcode_position + 1]
        else:
            left_operand = instructions[instructions[opcode_position + 1]]

        # Retrieve right operand value based on mode type
        if modes[1]:
            right_operand = instructions[opcode_position + 2]
        else:
            right_operand = instructions[instructions[opcode_position + 2]]

        # Retrieve operation output position
        output_position = instructions[opcode_position + 3]

        if opcode == 1:
            instructions[output_position] = left_operand + right_operand
        else:
            instructions[output_position] = left_operand * right_operand

        # Advance to next opcode position
        opcode_position += 4
    else:
        position = 0

        # Retrieve value based on mode type
        if modes[0]:
            position = opcode_position + 1
        else:
            position = instructions[opcode_position + 1]

        # Handle input/output instructions
        if opcode == 3:
            instructions[position] = input_val
        else:
            output.append(instructions[position])

        # Advance to next opcode position
        opcode_position += 2

# Traverse output to ensure all entries are zero,
#   with the exception of the last entry
for item in output:
    if item != 0 and item != output[len(output) - 1]:
        error_encountered = True

if error_encountered:
    print("Error encountered in output sequence...")
else:
    print(output[-1])

input_file.close()
