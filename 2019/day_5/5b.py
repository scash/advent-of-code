input_file = open("input.txt", "r")

# Read input and eliminate line ending
instructions = input_file.read().rstrip()

# Obtain list of values
instructions = instructions.split(",")

# Convert all values to integers
instructions = list(map(int, instructions))

output = list()
opcode_position, input_val = 0, 5

while instructions[opcode_position] != 99:

    # Default mode of "position" for all parameters
    modes = (0, 0, 0)

    # Retrieve opcode
    opcode = instructions[opcode_position]
    str_opcode = str(opcode)

    # Encountered opcode that contains parameter modes
    if len(str_opcode) > 1:

        # If applicable, fill missing position modes
        modes_opcode = str_opcode.zfill(5)

        opcode = int(modes_opcode[4])

        # Obtain parameter modes, however, transform to
        #   be left to right instead of right to left
        modes = tuple(map(int, reversed(modes_opcode[:3])))

    # Process non-input/output instructions
    if opcode not in [3, 4]:

        # Retrieve parameters of instruction based on mode
        if modes[0]:
            param1 = instructions[opcode_position + 1]
        else:
            param1 = instructions[instructions[opcode_position + 1]]

        if modes[1]:
            param2 = instructions[opcode_position + 2]
        else:
            param2 = instructions[instructions[opcode_position + 2]]

        # Handle write operations
        if opcode in [1, 2, 7, 8]:
            output_position = instructions[opcode_position + 3]

            # Handle write operations
            if opcode == 1:
                instructions[output_position] = param1 + param2
            elif opcode == 2:
                instructions[output_position] = param1 * param2
            elif opcode == 7:
                instructions[output_position] = 1 if param1 < param2 else 0
            else:
                instructions[output_position] = 1 if param1 == param2 else 0

        # Determine opcode position pointer
        if (opcode == 5 and param1 != 0) or (opcode == 6 and param1 == 0):
            opcode_position = param2
        elif opcode in [5, 6]:
            opcode_position += 3
        else:
            opcode_position += 4
    else:
        position = 0

        # Retrieve value based on mode type
        if modes[0]:
            position = opcode_position + 1
        else:
            position = instructions[opcode_position + 1]

        # Handle input/output instructions
        if opcode == 3:
            instructions[position] = input_val
        else:
            output.append(instructions[position])

        # Advance to next opcode position
        opcode_position += 2

print(output)

input_file.close()
