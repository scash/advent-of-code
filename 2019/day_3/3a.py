def plot_path(whole_path: list) -> list:
    x, y = 0, 0
    points = list()

    for path in whole_path:
        direction = path[0]
        length = int(path[1:])

        if direction == "R":

            for _ in range(length):
                x += 1
                points.append((x, y))

        elif direction == "L":

            for _ in range(length):
                x -= 1
                points.append((x, y))

        elif direction == "U":

            for _ in range(length):
                y += 1
                points.append((x, y))

        else:

            for _ in range(length):
                y -= 1
                points.append((x, y))

    return points



input_file = open("input.txt", "r")

w1_points, w2_points, manhattan_distances = list(), list(), list()

# Read input and eliminate line endings
w1 = input_file.readline().rstrip()
w2 = input_file.readline().rstrip()

# Obtain list of values for each wire path
w1 = w1.split(",")
w2 = w2.split(",")

# Plot path of both wires
w1_points = plot_path(w1)
w2_points = plot_path(w2)

# Determine points of intersection between the wires
intersections = set(w1_points) & set(w2_points)

# Obtain the manhattan distance for each intersection point
for x, y in intersections:
    manhattan_distances.append(abs(x) + abs(y))

# Output manhattan distance of closest intersection point
print(min(manhattan_distances))

input_file.close()
