def plot_path(whole_path: list) -> list:
    x, y, steps = 0, 0, 0
    points = list()

    for path in whole_path:
        direction = path[0]
        length = int(path[1:])

        if direction == "R":

            for _ in range(length):
                x += 1
                steps += 1
                points.append((x, y, steps))

        elif direction == "L":

            for _ in range(length):
                x -= 1
                steps += 1
                points.append((x, y, steps))

        elif direction == "U":

            for _ in range(length):
                y += 1
                steps += 1
                points.append((x, y, steps))

        else:

            for _ in range(length):
                y -= 1
                steps += 1
                points.append((x, y, steps))

    return points



input_file = open("input.txt", "r")

w1_points, w2_points, all_steps = list(), list(), list()

# Read input and eliminate line endings
w1 = input_file.readline().rstrip()
w2 = input_file.readline().rstrip()

# Obtain list of values for each wire path
w1 = w1.split(",")
w2 = w2.split(",")

# Plot path of both wires, including steps
w1_points = plot_path(w1)
w2_points = plot_path(w2)

# Obtain only coordinates of plotted paths
w1_coords = [(x, y) for x, y, steps in w1_points]
w2_coords = [(x, y) for x, y, steps in w2_points]

# Determine points of intersection between the wires
intersections = set(w1_coords) & set(w2_coords)

# Retrieve sum of steps for all instances of each intersection point
for x, y in intersections:

    steps = [w1_steps + w2_steps for w1_x, w1_y, w1_steps in w1_points if x == w1_x and y == w1_y
        for w2_x, w2_y, w2_steps in w2_points if w1_x == w2_x and w1_y == w2_y]

    all_steps.extend(steps)

print(min(all_steps))

input_file.close()
