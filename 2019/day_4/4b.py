eligible_passwords = list()

input_file = open("input.txt", "r")

# Read input and eliminate line ending
range_input = input_file.read().rstrip()

# Obtain list of values for eligible range
range_vals = range_input.split("-")

# Loop through the specified range
for i in range(int(range_vals[0]), int(range_vals[1])):
    repeats = dict()
    increase_rule = True

    # Create a numeric list from the looping counter
    digits = list(map(int, str(i)))

    for j in range(1, 6):

        # Does the password pass the increase/same rule?
        if digits[j] >= digits[j - 1]:

            # Ensure at least two adjacent digits are the same
            if digits[j] == digits[j - 1]:

                # Tally number of adjacent instances per digit
                if digits[j] not in repeats.keys():
                    repeats[digits[j]] = 1
                else:
                    repeats[digits[j]] += 1
        else:
            increase_rule = False
            break

    if increase_rule and repeats:

        # If at least one instance of adjacent digits was valid,
        #   (occurred only once) then an eligible password was encountered
        if 1 in repeats.values():
            eligible_passwords.append(i)

# Output number of eligible passwords
print(len(eligible_passwords))

input_file.close()
