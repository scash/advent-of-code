class Node:
    def __init__(self, name, parent = None, children = list()):
        self.name = name
        self.parent = parent
        self.children = children

    def add_parent(self, node):
        self.parent = node

    def add_child(self, node):
        self.children.append(node)

    def __str__(self):
        return self.name
