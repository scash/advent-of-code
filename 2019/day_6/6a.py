from node import Node

total_orbits = 0
orbits, orbits_tally = dict(), dict()

input_file = open("input.txt", "r")

# Loop through the file, one line at a time
for line in input_file:

    # Eliminate line ending
    objects = line.rstrip()

    obj1, obj2 = objects.split(")")

    if obj1 not in orbits:
        orbits[obj1] = Node(obj1)

    if obj2 not in orbits:
        orbits[obj2] = Node(obj2)

    # Add parent/child relationships
    orbits[obj1].add_child(orbits[obj2])
    orbits[obj2].add_parent(orbits[obj1])

for node in orbits.values():

    # Determine orbit count only if unknown
    if node not in orbits_tally:
        tally = 0
        loop_node = node.parent

        while loop_node:
            tally += 1
            loop_node = loop_node.parent

        orbits_tally[node] = tally

    total_orbits += orbits_tally[node]

print(total_orbits)

input_file.close()
