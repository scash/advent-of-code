from node import Node

def get_path(node: Node) -> list:
    path = list()

    while node:
        path.append(node)
        node = node.parent

    return path



transfers = 0
orbits =  dict()

input_file = open("input.txt", "r")

# Loop through the file, one line at a time
for line in input_file:

    # Eliminate line ending
    objects = line.rstrip()

    obj1, obj2 = objects.split(")")

    if obj1 not in orbits:
        orbits[obj1] = Node(obj1)

    if obj2 not in orbits:
        orbits[obj2] = Node(obj2)

    # Add parent/child relationships
    orbits[obj1].add_child(orbits[obj2])
    orbits[obj2].add_parent(orbits[obj1])

# Retrieve relevant nodes
node_you = orbits["YOU"]
node_san = orbits["SAN"]

# Retrieve orbital path of each node
you_path = get_path(node_you.parent)
san_path = get_path(node_san.parent)

# Traverse paths until first intersection is encountered
for i in range(len(you_path)):

    if you_path[i] in san_path:

        # Determine orbital transfers from intersection to Santa
        transfers += len(san_path[:san_path.index(you_path[i])])
        break
    else:
        transfers += 1

print(transfers)

input_file.close()
