data_list = list()
width, height, pixels = 25, 6, 0

input_file = open("input.txt", "r")

# Read input and eliminate line ending
image_data = input_file.read().rstrip()

# Convert all values to integers
image_data = list(map(int, image_data))

while len(image_data) > pixels:
    layer_list = list()

    for _ in range(height):
        row = image_data[pixels:(pixels + width)]

        pixels += width

        layer_list.append(row)

    data_list.append(layer_list)

# Reset layer list for reuse
layer_list = list()

# Render image by stacking pixel layers
for r in range(height):
    row = list()

    for c in range(width):
        for layer in range(len(data_list)):

            # Encountered a non-transparent pixel. Track
            # and move to the next set of pixel layers
            if data_list[layer][r][c] != 2:
                row.append(data_list[layer][r][c])
                break

    layer_list.append(row)

# Decode image
for r in range(height):
    for c in range(width):

        # Transform image into a human-readable form
        # by converting 1s to Xs and 0s to a single space
        p = str(layer_list[r][c]).replace("1", "X").replace("0", " ")

        # Output message, one pixel at a time
        print(p, end = "")

    # Break to the next line (row)
    print()

input_file.close()
