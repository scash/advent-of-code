data_list = list()
width, height = 25, 6
pixels, layers, zero_layer, one, two = 0, 0, 0, 0, 0

input_file = open("input.txt", "r")

# Read input and eliminate line ending
image_data = input_file.read().rstrip()

# Convert all values to integers
image_data = list(map(int, image_data))

# Set initial zero tally to maximum possible value
least_zeros = len(image_data)

while len(image_data) > pixels:
    zeros = 0
    layer_list = list()

    for _ in range(height):
        row = image_data[pixels:(pixels + width)]

        pixels += width
        zeros += row.count(0)

        layer_list.append(row)

    # Keep track of layer with the least amount of zeros
    if least_zeros >= zeros:
        zero_layer = layers
        least_zeros = zeros

    layers += 1
    data_list.append(layer_list)

# Tally 1s and 2s from the layer with the least 0s
for i in range(height):
    one += data_list[zero_layer][i].count(1)
    two += data_list[zero_layer][i].count(2)

# Output result
print(one * two)

input_file.close()
