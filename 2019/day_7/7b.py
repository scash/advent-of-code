from intcode import IntCode

from itertools import permutations

input_file = open("input.txt", "r")

# Read input and eliminate line ending
instructions = input_file.read().rstrip()

# Obtain list of values
instructions = instructions.split(",")

# Convert all values to integers
instructions = list(map(int, instructions))

all_output_signals = list()

# Loop through all possible permutations (5!) of 56789
for p in permutations(range(5, 10)):
    signal, thruster_output = 0, 0

    # Create an IntCode program for each amplifier
    #   with the proper phase setting
    amps = [IntCode(instructions[:], [phase_setting]) for phase_setting in p]

    while signal is not None:
        for i in range(len(amps)):
            amps[i].add_input(signal)

            signal = amps[i].get_next_output()

            if signal is not None:
                thruster_output = signal
            else:
                break

    # Store thruster signal
    all_output_signals.append(thruster_output)

print(max(all_output_signals))

input_file.close()
