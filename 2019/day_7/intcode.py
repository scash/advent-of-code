class IntCode:
    def __init__(self, instructions, inputs = list()):
        self.instructions = instructions
        self.inputs = inputs

        self.output = 0
        self.input_position = 0
        self.opcode_position = 0

        self.is_finished = False

    def get_next_output(self):

        while not self.is_finished:

            # Default mode of "position" for all parameters
            modes = (0, 0, 0)

            # Retrieve opcode
            opcode = self.instructions[self.opcode_position]
            str_opcode = str(opcode)

            # Determine if the program has finished
            if opcode == 99:
                self.is_finished = True
                break

            # Encountered opcode that contains parameter modes
            if len(str_opcode) > 1:

                # If applicable, fill missing position modes
                modes_opcode = str_opcode.zfill(5)

                opcode = int(modes_opcode[4])

                # Obtain parameter modes, however, transform to
                #   be left to right instead of right to left
                modes = tuple(map(int, reversed(modes_opcode[:3])))

            # Process non-input/output instructions
            if opcode not in [3, 4]:

                # Retrieve parameters of instruction based on mode
                if modes[0]:
                    param1 = self.instructions[self.opcode_position + 1]
                else:
                    param1 = self.instructions[self.instructions[self.opcode_position + 1]]

                if modes[1]:
                    param2 = self.instructions[self.opcode_position + 2]
                else:
                    param2 = self.instructions[self.instructions[self.opcode_position + 2]]

                # Handle write operations
                if opcode in [1, 2, 7, 8]:
                    output_position = self.instructions[self.opcode_position + 3]

                    # Handle write operations
                    if opcode == 1:
                        self.instructions[output_position] = param1 + param2
                    elif opcode == 2:
                        self.instructions[output_position] = param1 * param2
                    elif opcode == 7:
                        self.instructions[output_position] = 1 if param1 < param2 else 0
                    else:
                        self.instructions[output_position] = 1 if param1 == param2 else 0

                # Determine opcode position pointer
                if (opcode == 5 and param1 != 0) or (opcode == 6 and param1 == 0):
                    self.opcode_position = param2
                elif opcode in [5, 6]:
                    self.opcode_position += 3
                else:
                    self.opcode_position += 4
            else:
                position = 0

                # Retrieve value based on mode type
                if modes[0]:
                    position = self.opcode_position + 1
                else:
                    position = self.instructions[self.opcode_position + 1]

                # Advance to next opcode position
                self.opcode_position += 2

                # Handle input/output instructions
                if opcode == 3:
                    self.instructions[position] = self.inputs[self.input_position]

                    self.input_position += 1
                else:
                    self.output = self.instructions[position]

                    # Halt until further input is received
                    return self.output

        return None

    def add_input(self, val):
        self.inputs.append(val)
