input_file = open("input.txt", "r")

noun, verb = 0, 0
positions = list()

# Read input and eliminate line ending
default_values = input_file.read().rstrip()

# Obtain list of values
default_values = default_values.split(",")

# Convert all values to integers
default_values = list(map(int, default_values))

for i in range(100):
    for j in range(100):
        opcode_position = 0

        # Reset "memory" to default values via shallow copy
        positions = default_values[:]

        # Set "input" values for "address" 1 and 2
        positions[1] = i
        positions[2] = j

        # Execute "instructions" until a "halt instruction" is encountered
        while positions[opcode_position] != 99:

            # Retrieve opcode operator
            operator = positions[opcode_position]

            # Retrieve operands
            left_operand = positions[positions[opcode_position + 1]]
            right_operand = positions[positions[opcode_position + 2]]

            # Retrieve operation output position
            output_position = positions[opcode_position + 3]

            if operator == 1:
                positions[output_position] = left_operand + right_operand
            else:
                positions[output_position] = left_operand * right_operand

            # Advance to next opcode position
            opcode_position += 4

        if positions[0] == 19690720:
            verb = j
            break

    if positions[0] == 19690720:
        noun = i
        break

print((100 * noun) + verb)

input_file.close()
