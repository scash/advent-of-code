input_file = open("input.txt", "r")

opcode_position = 0

# Read input and eliminate line ending
positions = input_file.read().rstrip()

# Obtain list of values
positions = positions.split(",")

# Convert all values to integers
positions = list(map(int, positions))

# Replace two values from input, per instructions
positions[1] = 12
positions[2] = 2

while positions[opcode_position] != 99:

    # Retrieve opcode operator
    operator = positions[opcode_position]

    # Retrieve operands
    left_operand = positions[positions[opcode_position + 1]]
    right_operand = positions[positions[opcode_position + 2]]

    # Retrieve operation output position
    output_position = positions[opcode_position + 3]

    if operator == 1:
        positions[output_position] = left_operand + right_operand
    else:
        positions[output_position] = left_operand * right_operand

    # Advance to next opcode position
    opcode_position += 4

print(positions[0])

input_file.close()
