from collections import defaultdict

output = list()
instructions = defaultdict(int)
opcode_position, input_val, relative_base = 0, 2, 0

input_file = open("input.txt", "r")

# Read input and eliminate line ending
inst = input_file.read().rstrip()

# Obtain list of values
inst = inst.split(",")

# Convert all values to integers
inst = list(map(int, inst))

# Populate defaultdict from list, to handle
#   "memory beyond the initial program"
for i in range(len(inst)):
    instructions[i] = inst[i]

# Run program
while instructions[opcode_position] != 99:

    # Default mode of "position" for all parameters
    modes = (0, 0, 0)

    # Retrieve opcode
    opcode = instructions[opcode_position]
    str_opcode = str(opcode)

    # Encountered opcode that contains parameter modes
    if len(str_opcode) > 1:

        # If applicable, fill missing position modes
        modes_opcode = str_opcode.zfill(5)

        opcode = int(modes_opcode[4])

        # Obtain parameter modes, however, transform to
        #   be left to right instead of right to left
        modes = tuple(map(int, reversed(modes_opcode[:3])))

    # Process multi-parameter instructions
    if opcode not in [3, 4, 9]:

        # Retrieve parameters of instruction based on mode

        if modes[0] == 0:
            param1 = instructions[instructions[opcode_position + 1]]
        elif modes[0] == 1:
            param1 = instructions[opcode_position + 1]
        else:
            param1 = instructions[relative_base + instructions[opcode_position + 1]]

        if modes[1] == 0:
            param2 = instructions[instructions[opcode_position + 2]]
        elif modes[1] == 1:
            param2 = instructions[opcode_position + 2]
        else:
            param2 = instructions[relative_base + instructions[opcode_position + 2]]

        # Handle write operations
        if opcode in [1, 2, 7, 8]:
            output_position = 0

            if modes[2]:
                output_position = relative_base + instructions[opcode_position + 3]
            else:
                output_position = instructions[opcode_position + 3]

            if opcode == 1:
                instructions[output_position] = param1 + param2
            elif opcode == 2:
                instructions[output_position] = param1 * param2
            elif opcode == 7:
                instructions[output_position] = 1 if param1 < param2 else 0
            else:
                instructions[output_position] = 1 if param1 == param2 else 0

        # Determine opcode position pointer
        if (opcode == 5 and param1 != 0) or (opcode == 6 and param1 == 0):
            opcode_position = param2
        elif opcode in [5, 6]:
            opcode_position += 3
        else:
            opcode_position += 4
    else:
        position = 0

        # Retrieve position value based on mode type
        if modes[0] == 0:
            position = instructions[opcode_position + 1]
        elif modes[0] == 1:
            position = opcode_position + 1
        else:
            position = relative_base + instructions[opcode_position + 1]

        if opcode == 3:
            instructions[position] = input_val
        elif opcode == 4:
            output.append(instructions[position])
        else:
            relative_base += instructions[position]

        # Advance to next opcode position
        opcode_position += 2

print(output)

input_file.close()
