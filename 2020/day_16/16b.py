from functools import reduce

def check_position(the_content: list, the_fields: dict, position: int) -> bool:
    for line in the_content:
        if int(line.split(",")[position]) not in the_fields["valid_nums"]:
            return False

    return True



fields = dict()
valid_nums = set()
ticket_values = list()

input_file = open("input.txt", "r")

# Read input and eliminate line ending
content = input_file.read().rstrip()

content = content.split("\n\n")

range_content = content[0].split("\n")
my_ticket = "".join(content[1].split("\n")[1:]).split(",")
nearby_content = content[2].split("\n")[1:]

for i in range(len(range_content)):
    field_valid_nums = set()
    field_info = range_content[i].split(": ")

    ranges = field_info[1].split(" or ")

    first_range = ranges[0].split("-")
    second_range = ranges[1].split("-")

    for j in range(int(first_range[0]), int(first_range[1]) + 1):
        field_valid_nums.add(j)

    for k in range(int(second_range[0]), int(second_range[1]) + 1):
        field_valid_nums.add(k)

    fields[field_info[0]] = {"valid_nums": field_valid_nums}

    valid_nums.update(field_valid_nums)

# Shallow copy of ticket list to use for eliminating invalid tickets
nc_sc = nearby_content.copy()

# Remove invalid tickets from list
for line in nearby_content:
    nums = list(map(int, line.split(",")))

    for num in nums:
        if num not in valid_nums:
            nc_sc.remove(line)

nearby_content = nc_sc.copy()

# Append our ticket to ticket list
nearby_content.append(",".join(my_ticket))

# For each field, determine all potential valid positions
# e.g.  [0 1 1]
#       [1 1 1]
#       [0 0 1]
for item in fields:
    field_valid_positions = list()

    for position in range(len(my_ticket)):
        if check_position(nearby_content, fields[item], position):
            field_valid_positions.append(1)
        else:
            field_valid_positions.append(0)

    fields[item]["position_matrix"] = field_valid_positions

# Shallow copy of field data to use for eliminating fields as position is determined
f_sc = fields.copy()

# Determine the true position of each field,
# e.g.  [0 1 1]             [0 1 0]             [0 1 0]
#       [1 1 1]     ==>     [1 1 0]     ==>     [1 0 0]
#       [0 0 1]             [0 0 1]             [0 0 1]
while len(f_sc) > 0:
    field_keys = list(f_sc.keys())

    for i in range(len(f_sc)):
        item = field_keys[i]

        if sum(f_sc[item]["position_matrix"]) == 1:
            index = f_sc[item]["position_matrix"].index(1)

            the_fields = f_sc.copy()

            for field in the_fields:
                if field != item:
                    # NOTE: changes the value in memory that all shallow copies are referencing
                    the_fields[field]["position_matrix"][index] = 0
                else:
                    f_sc.pop(item)

            # Exit loop, as size of shallow copy has been altered
            break

# Works, but the looping below is much easier to follow
# ticket_values = [int(my_ticket[index]) for field in fields if "departure" in field for index in range(len(fields[field]["position_matrix"])) if fields[field]["position_matrix"][index] == 1]

# Obtain value of each departure field
for item in fields:
    if "departure" in item:
        for index in range(len(fields[item]["position_matrix"])):
            if fields[item]["position_matrix"][index] == 1:
                ticket_values.append(int(my_ticket[index]))
                break

print(reduce((lambda x, y: x * y), ticket_values))

input_file.close()
