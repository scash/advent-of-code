valid_nums = set()
invalid_nums = list()

input_file = open("input.txt", "r")

# Read input and eliminate line ending
content = input_file.read().rstrip()

content = content.split("\n\n")

range_content = content[0].split("\n")
nearby_content = content[2].split("\n")[1:]

for i in range(len(range_content)):
    ranges = range_content[i].split(": ")[1].split(" or ")

    first_range = ranges[0].split("-")
    second_range = ranges[1].split("-")

    for j in range(int(first_range[0]), int(first_range[1]) + 1):
        valid_nums.add(j)

    for k in range(int(second_range[0]), int(second_range[1]) + 1):
        valid_nums.add(k)

for line in nearby_content:
    nums = list(map(int, line.split(",")))

    for num in nums:
        if num not in valid_nums:
            invalid_nums.append(num)

print(sum(invalid_nums))

input_file.close()
