def get_result(nums: list) -> int:
    for i in range(len(nums)):
        for j in range(i + 1, len(nums)):
            if nums[i] + nums[j] == 2020:
                return nums[i] * nums[j]

    return 0



input_file = open("input.txt", "r")

# Read input and eliminate line ending
nums = input_file.read().rstrip()

# Obtain list of values
nums = nums.split("\n")

# Convert all values to integers
nums = list(map(int, nums))

print(get_result(nums))

input_file.close()
