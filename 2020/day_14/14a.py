memory_values = dict()
mask_one_num, mask_zero_num = 0, 0

input_file = open("input.txt", "r")

# Loop through the file, one line at a time
for line in input_file:

    # Eliminate line endings
    line = line.rstrip()

    parts = line.split()

    if "mask" in parts[0]:
        mask_one_num = int(parts[2].replace("X", "0"), 2)
        mask_zero_num = int(parts[2].replace("X", "1"), 2)
    else:
        value = int(parts[2])

        # Determine memory address
        start = parts[0].index("[") + 1
        end = parts[0].index("]")
        key = parts[0][start:end]

        memory_values[key] = (value | mask_one_num) & mask_zero_num

print(sum(memory_values.values()))

input_file.close()
