from itertools import product

mask = ""
mask_num = 0
memory_values = dict()

input_file = open("input.txt", "r")

# Loop through the file, one line at a time
for line in input_file:

    # Eliminate line endings
    line = line.rstrip()

    parts = line.split()

    if "mask" in parts[0]:
        mask = parts[2]
        mask_num = int(parts[2].replace("X", "0"), 2)
    else:
        value = int(parts[2])

        # Determine memory address
        start = parts[0].index("[") + 1
        end = parts[0].index("]")
        orig_address = int(parts[0][start:end])

        # Convert int to binary, post bitwise OR
        mask_value_pre_float = bin(orig_address | mask_num).replace("0b", "").zfill(36)

        float_tally = mask.count("X")

        # Obtain all combinations of "(0, 1)" of length "float_tally"
        for combo in list(product([0, 1], repeat=float_tally)):
            index = -1
            new_address = list(mask_value_pre_float)

            for j in range(len(combo)):
                index = mask.index("X", index + 1)
                new_address[index] = str(combo[j])

            new_address = "".join(new_address)
            memory_values[int(new_address, 2)] = value

print(sum(memory_values.values()))

input_file.close()
