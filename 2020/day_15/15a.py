input_file = open("input.txt", "r")

# Read input and eliminate line ending
nums = input_file.readline().rstrip()

# Obtain list of values
nums = nums.split(",")

# Convert all values to integers
nums = list(map(int, nums))

prev = nums[len(nums) - 1]
nums = nums[:-1]
counter = len(nums) + 1

while len(nums) < 2020:
    num = 0

    if prev in nums:
        prev_instance = len(nums) - nums[::-1].index(prev)
        num = counter - prev_instance

    nums.append(prev)

    prev = num
    counter += 1

print(nums[len(nums) - 1])

input_file.close()
