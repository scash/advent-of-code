valid = 0
required_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n\n")

for i in range(len(lines)):
    required_field_tally = 0

    # Traverse through all required fields
    # to determine if they're present in the passport
    for j in range(len(required_fields)):

        # A required field encountered in passport
        if required_fields[j] in lines[i]:
            required_field_tally += 1
        else:
            break

    # Passport contains all required fields and it's therefore valid
    if required_field_tally == len(required_fields):
        valid += 1

print(valid)

input_file.close()
