valid = 0
required_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n\n")

for i in range(len(lines)):
    required_field_tally = 0

    # Traverse through all required fields
    # to determine if they're present in the passport
    for j in range(len(required_fields)):

        # A required field encountered in passport
        if required_fields[j] in lines[i]:
            required_field_tally += 1
        else:
            break

    # Passport contains all required fields.
    # Ensure data is valid for each required field.
    if required_field_tally == len(required_fields):
        valid_field_tally = 0
        items = lines[i].split()

        for k in range(len(items)):
            field = items[k].split(":")

            if field[0] == "byr":
                value = int(field[1])

                if len(field[1]) == 4 and value >= 1920 and value <= 2002:
                    valid_field_tally += 1
                else:
                    break
            elif field[0] == "iyr":
                value = int(field[1])

                if len(field[1]) == 4 and value >= 2010 and value <= 2020:
                    valid_field_tally += 1
                else:
                    break
            elif field[0] == "eyr":
                value = int(field[1])

                if len(field[1]) == 4 and value >= 2020 and value <= 2030:
                    valid_field_tally += 1
                else:
                    break
            elif field[0] == "hgt":
                if field[1].find("cm") != -1:
                    int_value = int(field[1][:field[1].find("cm")])

                    if int_value >= 150 and int_value <= 193:
                        valid_field_tally += 1
                    else:
                        break

                if field[1].find("in") != -1:
                    int_value = int(field[1][:field[1].find("in")])

                    if int_value >= 59 and int_value <= 76:
                        valid_field_tally += 1
                    else:
                        break
            elif field[0] == "hcl":
                valid_char_tally = 0

                if field[1][0] == "#":

                    for i in range(1, len(field[1])):
                        if field[1][i] in ("0123456789abcdef"):
                            valid_char_tally += 1

                    if valid_char_tally == 6:
                        valid_field_tally += 1
                    else:
                        break
                else:
                    break
            elif field[0] == "ecl":
                if field[1] in ("amb", "blu", "brn", "gry", "grn", "hzl", "oth"):
                    valid_field_tally += 1
                else:
                    break
            else:
                if len(field[1]) == 9 and field[1].isnumeric():
                    valid_field_tally += 1

        # All fields are valid
        if valid_field_tally == len(required_fields):
            valid += 1

print(valid)

input_file.close()
