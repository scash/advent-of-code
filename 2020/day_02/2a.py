valid = 0

input_file = open("input.txt", "r")

# Loop through the file, one line at a time
for line in input_file:

    # Eliminate line endings
    line = line.rstrip()

    line = line.split(":")

    password = line[1].lstrip()

    line = line[0].split()

    char = line[1]

    nums = line[0].split("-")
    low = int(nums[0])
    high = int(nums[1])

    # Determine the number of occurrences
    # of the specified character within the password
    times = password.count(char)

    if times >= low and times <= high:
        valid += 1

print(valid)

input_file.close()
