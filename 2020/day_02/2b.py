valid = 0

input_file = open("input.txt", "r")

# Loop through the file, one line at a time
for line in input_file:
    char_occurrence = 0

    # Eliminate line endings
    line = line.rstrip()

    line = line.split(":")

    password = line[1].lstrip()

    line = line[0].split()

    char = line[1]

    nums = line[0].split("-")
    low = int(nums[0])
    high = int(nums[1])

    # Is the character present in the first location?
    if password[low - 1] == char:
        char_occurrence += 1

    # Is the character present in the second location?
    if password[high - 1] == char:
        char_occurrence += 1

    # Valid only if one of the positions had the specified character
    if char_occurrence == 1:
        valid += 1

print(valid)

input_file.close()
