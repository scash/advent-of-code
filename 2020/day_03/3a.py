trees, depth = 0, 0

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n")

for i in range(len(lines)):
    depth += 3

    # No need to proceed if on last line
    if i == (len(lines) - 1):
        break

    # Find the remainder to determine
    # location within the repeating pattern
    location = depth % len(lines[i + 1])

    if lines[i + 1][location] == "#":
        trees += 1

print(trees)

input_file.close()
