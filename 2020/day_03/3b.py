from functools import reduce

multipliers = list()
slopes = [[1,1], [3,1], [5,1], [7,1], [1,2]]

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n")

for slope in slopes:
    trees, depth = 0, 0

    # Determine trees encountered within the specified slope
    for i in range(0, len(lines), slope[1]):
        depth += slope[0]

        # No need to proceed if on last line
        # or if next slope exceeds grid boundry
        if i == (len(lines) - 1) or (i + slope[1]) > (len(lines) - 1):
            break

        # Find the remainder to determine
        # location within the repeating pattern
        location = depth % len(lines[i + slope[1]])

        if lines[i + slope[1]][location] == "#":
            trees += 1

    multipliers.append(trees)

print(reduce((lambda x, y: x * y), multipliers))

input_file.close()
