x, y = 0, 0
facing = (1, 0)
rotations = {
    (1, 0): {90: (0, 1), 180: (-1, 0), 270: (0, -1)},
    (0, 1): {90: (-1, 0), 180: (0, -1), 270: (1, 0)},
    (-1, 0): {90: (0, -1), 180: (1, 0), 270: (0, 1)},
    (0, -1): {90: (1, 0), 180: (0, 1), 270: (-1, 0)}
}

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n")

for i in range(len(lines)):
    action = lines[i][:1]
    num = int(lines[i][1:])

    if action == "N":
        y += num
    elif action == "S":
        y -= num
    elif action == "E":
        x += num
    elif action == "W":
        x -= num
    elif action == "L":
        facing = rotations[facing][num]
    elif action == "R":
        if num == 90:
            num = 270
        elif num == 270:
            num = 90

        facing = rotations[facing][num]
    else:
        if facing[0] != 0:
            x += (facing[0] * num)
        else:
            y += (facing[1] * num)

print(abs(x) + abs(y))

input_file.close()
