def detect_rotation_change(vals: dict) -> dict:
    if vals["units"] < 0:
        vals["units"] = abs(vals["units"])
        vals["facing"] = rotations[vals["facing"]][180]

    return vals



x_ship, y_ship = 0, 0
wp_vals = [
    {"facing": (1, 0), "units": 10},
    {"facing": (0, 1), "units": 1}
]
rotations = {
    (1, 0): {90: (0, 1), 180: (-1, 0), 270: (0, -1)},
    (0, 1): {90: (-1, 0), 180: (0, -1), 270: (1, 0)},
    (-1, 0): {90: (0, -1), 180: (1, 0), 270: (0, 1)},
    (0, -1): {90: (1, 0), 180: (0, 1), 270: (-1, 0)}
}

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n")

for i in range(len(lines)):
    action = lines[i][:1]
    num = int(lines[i][1:])

    if action == "N":
        if wp_vals[0]["facing"][1] != 0:
            wp_vals[0]["units"] += (num * wp_vals[0]["facing"][1])
            wp_vals[0] = detect_rotation_change(wp_vals[0])
        else:
            wp_vals[1]["units"] += (num * wp_vals[1]["facing"][1])
            wp_vals[1] = detect_rotation_change(wp_vals[1])
    elif action == "S":
        if wp_vals[0]["facing"][1] != 0:
            wp_vals[0]["units"] -= (num * wp_vals[0]["facing"][1])
            wp_vals[0] = detect_rotation_change(wp_vals[0])
        else:
            wp_vals[1]["units"] -= (num * wp_vals[1]["facing"][1])
            wp_vals[1] = detect_rotation_change(wp_vals[1])
    elif action == "E":
        if wp_vals[0]["facing"][0] != 0:
            wp_vals[0]["units"] += (num * wp_vals[0]["facing"][0])
            wp_vals[0] = detect_rotation_change(wp_vals[0])
        else:
            wp_vals[1]["units"] += (num * wp_vals[1]["facing"][0])
            wp_vals[1] = detect_rotation_change(wp_vals[1])
    elif action == "W":
        if wp_vals[0]["facing"][0] != 0:
            wp_vals[0]["units"] -= (num * wp_vals[0]["facing"][0])
            wp_vals[0] = detect_rotation_change(wp_vals[0])
        else:
            wp_vals[1]["units"] -= (num * wp_vals[1]["facing"][0])
            wp_vals[1] = detect_rotation_change(wp_vals[1])
    elif action == "L":
        wp_vals[0]["facing"] = rotations[wp_vals[0]["facing"]][num]
        wp_vals[1]["facing"] = rotations[wp_vals[1]["facing"]][num]
    elif action == "R":
        if num == 90:
            num = 270
        elif num == 270:
            num = 90

        wp_vals[0]["facing"] = rotations[wp_vals[0]["facing"]][num]
        wp_vals[1]["facing"] = rotations[wp_vals[1]["facing"]][num]   
    else:
        if wp_vals[0]["facing"][0] != 0:
            x_ship += ((wp_vals[0]["facing"][0] * num) * wp_vals[0]["units"])
        else:
            y_ship += ((wp_vals[0]["facing"][1] * num) * wp_vals[0]["units"])

        if wp_vals[1]["facing"][0] != 0:
            x_ship += ((wp_vals[1]["facing"][0] * num) * wp_vals[1]["units"])
        else:
            y_ship += ((wp_vals[1]["facing"][1] * num) * wp_vals[1]["units"])

print(abs(x_ship) + abs(y_ship))

input_file.close()
