def get_bag_count(look_for: str, the_input: dict, tally: int) -> int:

    for k, v in the_input[look_for].items():
        tally += v + (v * get_bag_count(k, the_input, 0))

    return tally



bags = dict()
bag_name = "shiny gold"

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n")

for i in range(len(lines)):
    nums = "123456789"
    bag_content = dict()

    bag_info = lines[i].split(" bags contain ")

    # Create a dictionary that represents the content of the bag.
    # The key represents the name of each bag and the value is the quantity.
    for item in bag_info[1].split(","):
        for num in nums:
            if num in item:
                start = item.find(num)
                end = item.find("bag")

                # Retrieve bag name and assign its numeric value
                bag_content[item[start + 2:end - 1]] = int(num)
                break

    # Add bag content of specified bag
    bags[bag_info[0]] = bag_content

print(get_bag_count(bag_name, bags, 0))

input_file.close()
