def get_bag_content(look_for: str, the_input: list, the_set: set) -> set:

    for i in range(len(the_input)):
        bag_info = the_input[i].split(" bags contain ")

        if look_for in bag_info[1]:
            the_set.add(bag_info[0])
            the_set = get_bag_content(bag_info[0], the_input, the_set)

    return the_set



bags = set()
bag_name = "shiny gold"

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n")

bags = get_bag_content(bag_name, lines, bags)

print(len(bags))

input_file.close()
