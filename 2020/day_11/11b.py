def get_point(the_matrix: dict, x: int, y: int, x_offset: int, y_offset: int, vals: dict) -> dict:
    while True:
        x += x_offset
        y += y_offset

        if (x, y) in the_matrix.keys():
            if the_matrix[(x, y)] == ".":
                continue

            if the_matrix[(x, y)] in vals["allowed"]:
                vals["tally"] += 1

            break
        else:
            vals["OutOfBounds"] += 1
            break

    return vals

def reqs_tally(the_matrix: dict, x: int, y: int, the_type: str) -> int:
    empty, occupied = 0, 0
    vals = {
            "#": {"tally": 0, "OutOfBounds": 0, "allowed": "#"},
            "L": {"tally": 0, "OutOfBounds": 0, "allowed": "L"}
    }

    # Top-Left
    vals[the_type] = get_point(the_matrix, x, y, -1, -1, vals[the_type])

    # Top-Center
    vals[the_type] = get_point(the_matrix, x, y, -1, 0, vals[the_type])

    # Top-Right
    vals[the_type] = get_point(the_matrix, x, y, -1, 1, vals[the_type])

    # Middle-Left
    vals[the_type] = get_point(the_matrix, x, y, 0, -1, vals[the_type])

    # Middle-Right
    vals[the_type] = get_point(the_matrix, x, y, 0, 1, vals[the_type])

    # Bottom-Left
    vals[the_type] = get_point(the_matrix, x, y, 1, -1, vals[the_type])

    # Bottom-Center
    vals[the_type] = get_point(the_matrix, x, y, 1, 0, vals[the_type])

    # Bottom-Right
    vals[the_type] = get_point(the_matrix, x, y, 1, 1, vals[the_type])

    occupied = vals[the_type]["tally"]
    empty = vals[the_type]["tally"] + vals[the_type]["OutOfBounds"]

    return empty if the_type == "L" else occupied

def populate_matrix(the_data: list) -> dict:
    output = dict()

    for i in range(len(the_data)):
        for j in range(len(the_data[i])):
            output[(i, j)] = the_data[i][j]

    return output

def get_count(the_data: list) -> int:
    num = 0

    for i in range(len(the_data)):
        for j in range(len(the_data[i])):
            if the_data[i][j] == "#":
                num += 1

    return num



matrix = dict()
prev_iteration, latest_iteration = list(), list()

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n")

prev_iteration = lines.copy()

while True:
    matrix = populate_matrix(prev_iteration)
    latest_iteration = [[""] * len(prev_iteration[0]) for _ in range(len(prev_iteration))]

    for i in range(len(prev_iteration)):
        for j in range(len(prev_iteration[i])):
            latest_iteration[i][j] = prev_iteration[i][j]

            if prev_iteration[i][j] != ".":
                num = reqs_tally(matrix, i, j, prev_iteration[i][j])

                if num >= 5 and prev_iteration[i][j] == "#":
                    latest_iteration[i][j] = "L"
                if num == 8 and prev_iteration[i][j] == "L":
                    latest_iteration[i][j] = "#"

    if (prev_iteration == latest_iteration):
        break
    else:
        prev_iteration = latest_iteration.copy()

print(get_count(latest_iteration))

input_file.close()
