def sum_of_two(nums: list, num: int) -> bool:
    for j in range(len(nums)):
        for k in range(j + 1, len(nums)):
            if nums[j] + nums[k] == num:
                return True

    return False



num = 0
nums = list()

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n")

# Convert all values to integers
lines = list(map(int, lines))

# Create preamble
nums = lines[:25]

for i in range(25, len(lines)):
    num = lines[i]

    if sum_of_two(nums, num):
        nums = nums[1:]
        nums.append(num)
    else:
        break

print(num)

input_file.close()
