times = dict()

input_file = open("input.txt", "r")

timestamp = int(input_file.readline().rstrip())
buses = input_file.readline().rstrip().replace("x,", "")

# Obtain list of values
buses = buses.split(",")

# Convert all values to integers
buses = list(map(int, buses))

for i in range(len(buses)):
    time = (timestamp // buses[i]) * buses[i]

    if time != timestamp:
        times[buses[i]] = time + buses[i]
    else:
        times[buses[i]] = time
        break

closest = min(times.values())

bus_id = [k for k, v in times.items() if v == closest][0]

print((closest - timestamp) * bus_id)

input_file.close()
