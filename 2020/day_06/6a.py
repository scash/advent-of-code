tally = 0

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n\n")

for i in range(len(lines)):
    unique_questions = set()

    # Obtain all questions from the group
    all_questions = [question for question in lines[i] if question != "\n"]

    # Add group questions to our set, which eliminates duplicates
    unique_questions.update(*all_questions)

    tally += len(unique_questions)

print(tally)

input_file.close()
