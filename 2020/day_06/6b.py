tally = 0

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n\n")

for i in range(len(lines)):
    unique_questions = set()

    # Get number of passengers in the group
    group_passenger_tally = len(lines[i].split("\n"))

    # Obtain all questions from the group
    all_questions = [question for question in lines[i] if question != "\n"]

    # Add group questions to our set, which eliminates duplicates
    unique_questions.update(*all_questions)

    for question in unique_questions:
        if all_questions.count(question) == group_passenger_tally:
            tally += 1

print(tally)

input_file.close()
