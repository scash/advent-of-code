num = 0
nums = {1: 0, 2: 0, 3: 1}

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n")

# Convert all values to integers
lines = list(map(int, lines))

lines = sorted(lines)

for i in range(len(lines)):
    nums[lines[i] - num] += 1

    num = lines[i]

print(nums[1] * nums[3])

input_file.close()
