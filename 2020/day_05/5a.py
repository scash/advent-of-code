seats = list()

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n")

for i in range(len(lines)):

    # Obtain binary representation of row and column
    row = lines[i][:7].replace("F", "0").replace("B", "1")
    col = lines[i][7:10].replace("L", "0").replace("R", "1")

    # Convert from binary to int
    row = int(row, 2)
    col = int(col, 2)

    seats.append(row * 8 + col)

print(max(seats))

input_file.close()
