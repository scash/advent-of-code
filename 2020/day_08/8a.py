visited = list()
accumulator, idx = 0, 0

input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n")

while idx not in visited:
    visited.append(idx)

    instruction = lines[idx].split()

    if instruction[0] == "acc":
        idx += 1
        accumulator += int(instruction[1])
    elif instruction[0] == "jmp":
        idx += int(instruction[1])
    else:
        idx += 1

print(accumulator)

input_file.close()
