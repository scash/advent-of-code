input_file = open("input.txt", "r")

# Read input and eliminate line ending
lines = input_file.read().rstrip()

# Obtain list of values
lines = lines.split("\n")

for i in range(len(lines)):
    accumulator, idx = 0, 0
    visited, copied = list(), list()

    instruction = lines[i].split()

    if instruction[0] == "jmp":
        copied = lines.copy()
        copied[i] = lines[i].replace("jmp", "nop")
    elif instruction[0] == "nop":
        copied = lines.copy()
        copied[i] = lines[i].replace("nop", "jmp")
    else:
        continue

    while idx not in visited and idx < len(lines):
        visited.append(idx)

        instruction = copied[idx].split()

        if instruction[0] == "acc":
            idx += 1
            accumulator += int(instruction[1])
        elif instruction[0] == "jmp":
            idx += int(instruction[1])
        else:
            idx += 1

    # If we reached the end, then the proper instruction was modified
    if idx == len(lines):
        break

print(accumulator)

input_file.close()
