<?php

require dirname(__FILE__) . "/../Helper/FileReader.php";
use Helper\FileReader;

try {
	$arrSize = array();
	$arrFabric = array();
	$arrLocation = array();
	$intConflictSquares = 0;

	$inputFile = new FileReader("input.txt");

	$data = $inputFile->parseFile();

	foreach($data as $row) {
		// Obtain distance from edge
		$arrLocation = explode(",", $row);
		$arrLocation[0] = intval(explode(" ", $arrLocation[0])[2]);
		$arrLocation[1] = intval(explode(":", $arrLocation[1])[0]);

		// Obtain size of rectangle
		$arrSize = explode("x", $row);
		$arrSize[0] = intval(explode(" ", $arrSize[0])[3]);
		$arrSize[1] = intval($arrSize[1]);

		// Claim vertically
		for($i = $arrLocation[1]; ($arrLocation[1] + $arrSize[1]) > $i; $i++) {

			// Claim horizontally
			for($j = $arrLocation[0]; ($arrLocation[0] + $arrSize[0]) > $j; $j++) {
				if(isset($arrFabric[$i][$j])) {
					$arrFabric[$i][$j] = "X";
				} else {
					$arrFabric[$i][$j] = 1;
				}
			}
		}
	}

	// Determine number of conflicts encountered
	foreach($arrFabric as $row) {
		foreach($row as $square) {
			if($square == "X") {
				$intConflictSquares += 1;
			}
		}
	}

	echo $intConflictSquares;

	$inputFile->deleteFileInMemory();
} catch(\Exception $e) {
	// File not found!
}

?>