<?php

namespace Helper;

use SplFileObject;

/**
 * An object representation of a file
 *
 * @author Santos Cash
 */
class FileReader {

	/**
	 * The file.
	 *
	 * @var	\SplFileObject
	 */
	private $file;

	/**
	 * Constructor that creates a new file object.
	 *
	 * @param	string	$filePath	The path to the file
	 * @param	string	$mode	The mode in which to open the file
	 * @throws	Exception	When the file cannot be found
	 */
	public function __construct(string $filePath, string $mode = "r") {
		if(!file_exists($filePath)) {
			throw new \Exception("File not found!");
		}

		$this->file = new SplFileObject($filePath, $mode);
	}

	/**
	 * Parse the file object. Each line is returned in its raw state.
	 *
	 * @return	array	An array containing the data parsed from the file
	 */
	public function parseFile(): array {
		$data = array();

		// Ensure blank lines are skipped
		$this->file->setFlags(SplFileObject::READ_AHEAD | SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);

		while (!$this->file->eof()) {
			$data[] = $this->file->fgets();
		}

		// Rewind to 1st line to ensure proper processing on future calls
		$this->file->rewind();

		return $data;
	}

	/**
	 * Parse the file object. Assumes file is in CSV format.
	 *
	 * @return	array	An array containing the data parsed from the CSV file
	 */
	public function parseFileAsCsv(): array {
		$data = array();

		// Ensure blank lines are skipped
		$this->file->setFlags(SplFileObject::READ_CSV | SplFileObject::READ_AHEAD | SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);

		foreach($this->file as $row) {
			$data[] = $row;
		}

		// Rewind to 1st line to ensure proper processing on future calls
		$this->file->rewind();

		return $data;
	}

	public function deleteFileInMemory() {
		$this->file =  NULL;
	}
}

?>