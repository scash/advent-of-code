<?php

// use SplFileObject;

$file = new SplFileObject("test_input.txt", "r");

$data = array();

// Ensure blank lines are skipped
$file->setFlags(SplFileObject::READ_AHEAD | SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);

while (!$file->eof()) {
	$data[] = $file->fgets();
}

foreach($data as $row) {
	echo $row . "\n";
}

for($i = 0; count($data) > $i; $i++) {
	echo $data[$i] . "\n";
}

?>