<?php

require dirname(__FILE__) . "/../Helper/FileReader.php";
use Helper\FileReader;

try {
	$intGuard = 0;
	$intMinute = 0;
	$intStartSleep = 0;
	$arrGuards = array();
	$arrContent = array();
	$intSleepistGuard = 0;
	$intMostSleptTally = 0;

	$inputFile = new FileReader("input.txt");

	$data = $inputFile->parseFile();

	for($i = 0; count($data) > $i; $i++) {
		$arrContent[$i]["date"] = explode("[", explode("]", $data[$i])[0])[1];
		$arrContent[$i]["minute"] = intval(explode("]", explode(":", $data[$i])[1])[0]);
		$arrContent[$i]["action"] = explode("] ", $data[$i])[1];
	}

	// Sort data chronologically
	usort($arrContent, function($a, $b) {
		return strcmp($a["date"], $b["date"]);
	});

	foreach($arrContent as $entry) {
		// Guard has began shift
		if(strpos($entry["action"], "#") !== false) {
			$intGuard = intval(explode(" ", explode("#", $entry["action"])[1])[0]);
			continue;
		}

		// Guard has fallen asleep
		if(strpos($entry["action"], "falls asleep") !== false) {
			$intStartSleep = $entry["minute"];
			continue;
		}

		// Guard has awaken, tally sleep time
		if(strpos($entry["action"], "wakes up") !== false) {

			for($i = $intStartSleep; $entry["minute"] > $i; $i++) {
				if(isset($arrGuards[$intGuard][$i])) {
					$arrGuards[$intGuard][$i] += 1;
					$arrGuards[$intGuard]["tally"] += 1;
				} else {
					$arrGuards[$intGuard][$i] = 1;

					if(isset($arrGuards[$intGuard]["tally"])) {
						$arrGuards[$intGuard]["tally"] += 1;
					} else {
						$arrGuards[$intGuard]["tally"] = 1;
						$arrGuards[$intGuard]["id"] = $intGuard;
					}
				}
			}

			if($arrGuards[$intGuard]["tally"] > $intMostSleptTally) {
				$intSleepistGuard = $intGuard;
				$intMostSleptTally = $arrGuards[$intGuard]["tally"];
			}
		}
	}

	// Eliminate non-minute entries in array
	unset($arrGuards[$intSleepistGuard]["id"]);
	unset($arrGuards[$intSleepistGuard]["tally"]);

	// Obtain the minute the guard slept the most
	$intMinute = array_flip($arrGuards[$intSleepistGuard])[max($arrGuards[$intSleepistGuard])];

	echo $intSleepistGuard * $intMinute;

	$inputFile->deleteFileInMemory();
} catch(\Exception $e) {
	// File not found!
}

?>