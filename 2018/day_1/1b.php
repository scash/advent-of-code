<?php

require dirname(__FILE__) . "/../Helper/FileReader.php";
use Helper\FileReader;

try {
	$intFrequency = 0;
	$bolLoopState = true;
	$arrFrequencies = array(0);

	$inputFile = new FileReader("input.txt");

	$data = $inputFile->parseFile();

	while($bolLoopState) {
		foreach($data as $row) {
			$intFrequency += intval($row);

			if(in_array($intFrequency, $arrFrequencies)) {
				$bolLoopState = false;
				break;
			}

			$arrFrequencies[] = $intFrequency;
		}
	}

	echo $intFrequency;

	$inputFile->deleteFileInMemory();
} catch(\Exception $e) {
	// File not found!
}

?>