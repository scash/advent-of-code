<?php

require dirname(__FILE__) . "/../Helper/FileReader.php";
use Helper\FileReader;

try {
	$intFrequency = 0;

	$inputFile = new FileReader("input.txt");

	$data = $inputFile->parseFile();

	foreach($data as $row) {
		$intFrequency += intval($row);
	}

	echo $intFrequency;

	$inputFile->deleteFileInMemory();
} catch(\Exception $e) {
	// File not found!
}

?>