<?php

require dirname(__FILE__) . "/../Helper/FileReader.php";
use Helper\FileReader;

try {
	$charCurr = "";
	$charNext = "";
	$intShortestLength = 0;
	$modifiedData = array();
	$intCurrentPosition = 0;

	$inputFile = new FileReader("input.txt");

	$data = $inputFile->parseFile();
	$data = str_split($data[0]);

	$intTally = count($data);
	$arrLetters = range("a", "z");

	foreach($arrLetters as $charLetter) {
		// Eliminate alphabetic character from string array
		$modifiedData = array_values(array_diff($data, array($charLetter, strtoupper($charLetter))));

		while(true) {
			if($intCurrentPosition < 0) {
				$intCurrentPosition = 0;
			}

			if(count($modifiedData) > ($intCurrentPosition + 1)) {
				$charCurr = $modifiedData[$intCurrentPosition];
				$charNext = $modifiedData[$intCurrentPosition + 1];
			} else {
				break;
			}

			if(strtolower($charCurr) == strtolower($charNext)) {
				if((ctype_lower($charCurr) && ctype_upper($charNext))
					|| (ctype_upper($charCurr) && ctype_lower($charNext))) {

					array_splice($modifiedData, $intCurrentPosition, 2);
					$intCurrentPosition -= 1;
					continue;
				}
			}

			$intCurrentPosition += 1;
		}

		if($intTally > count($modifiedData)) {
			$intTally = count($modifiedData);
			$intShortestLength = $intTally;
		}

		$intCurrentPosition = 0;
	}

	echo $intShortestLength;

	$inputFile->deleteFileInMemory();
} catch(\Exception $e) {
	// File not found!
}

?>