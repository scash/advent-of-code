<?php

require dirname(__FILE__) . "/../Helper/FileReader.php";
use Helper\FileReader;

try {
	$charCurr = "";
	$charNext = "";
	$intCurrentPosition = 0;

	$inputFile = new FileReader("input.txt");

	$data = $inputFile->parseFile();
	$data = str_split($data[0]);

	while(true) {
		if($intCurrentPosition < 0) {
			$intCurrentPosition = 0;
		}

		if(count($data) > ($intCurrentPosition + 1)) {
			$charCurr = $data[$intCurrentPosition];
			$charNext = $data[$intCurrentPosition + 1];
		} else {
			break;
		}

		if(strtolower($charCurr) == strtolower($charNext)) {
			if((ctype_lower($charCurr) && ctype_upper($charNext))
				|| (ctype_upper($charCurr) && ctype_lower($charNext))) {

				array_splice($data, $intCurrentPosition, 2);
				$intCurrentPosition -= 1;
				continue;
			}
		}

		$intCurrentPosition += 1;
	}

	echo count($data);

	$inputFile->deleteFileInMemory();
} catch(\Exception $e) {
	// File not found!
}

?>