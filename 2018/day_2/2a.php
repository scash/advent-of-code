<?php

require dirname(__FILE__) . "/../Helper/FileReader.php";
use Helper\FileReader;

try {
	$intDouble = 0;
	$intTriple = 0;
	$arrChars = array();

	$inputFile = new FileReader("input.txt");

	$data = $inputFile->parseFile();

	foreach($data as $row) {
		for($i = 0; strlen($row) > $i; $i++) {
			if(isset($arrChars[$row[$i]])) {	
				$arrChars[$row[$i]] += 1;
			} else {
				$arrChars[$row[$i]] = 1;
			}
		}

		if(in_array(2, array_values($arrChars))) {
			$intDouble += 1;
		}

		if(in_array(3, array_values($arrChars))) {
			$intTriple += 1;
		}

		// Reset array
		$arrChars = array();
	}

	echo $intDouble * $intTriple;

	$inputFile->deleteFileInMemory();
} catch(\Exception $e) {
	// File not found!
}

?>