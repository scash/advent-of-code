<?php

require dirname(__FILE__) . "/../Helper/FileReader.php";
use Helper\FileReader;

try {
	$intDiff = 0;
	$strOutput = "";
	$intCounter = 1;

	$inputFile = new FileReader("input.txt");

	$data = $inputFile->parseFile();

	foreach($data as $row) {

		// Begin check at the last row, searching forward
		for($i = $intCounter; count($data) > $i; $i++) {
			for($j = 0; strlen($row) > $j; $j++) {
				if($row[$j] != $data[$i][$j]) {
					$intDiff += 1;

					if($intDiff > 1) {
						break;
					}
				}
			}

			if($intDiff != 1) {
				$intDiff = 0;
			} else {
				// We found the two semi-identical strings

				// Remove the differing char 
				for($k = 0; strlen($row) > $k; $k++) {
					if($row[$k] == $data[$i][$k]) {
						$strOutput .= $row[$k];
					}
				}

				// No need to continue searching
				break 2;
			}
		}

		$intCounter++;
	}

	echo $strOutput;

	$inputFile->deleteFileInMemory();
} catch(\Exception $e) {
	// File not found!
}

?>